using System;
using System.Collections.Generic;
using System.Linq;

namespace TheApp
{
    class Program
    {
        public static List<string> DataFilesList = new List<string>
        {
            // "a_an_example.in.txt",
            "b_better_start_small.in.txt",
            "c_collaboration.in.txt",
            "d_dense_schedule.in.txt",
            "e_exceptional_skills.in.txt",
            "f_find_great_mentors.in.txt"
        };

        static void Main()
        {
            foreach (var ooga in DataFilesList)
            {
                var fileName = ooga;

                var dataSet = InputOutput.ReadData($"data/{fileName}");

                var result = new Result();

                var countOfEmptyCycles = 0;
                var simulationDays = 0;

                while (dataSet.Projects.Any(x => !x.Expired))
                {
                    ScoreProjects(dataSet.Projects, simulationDays);

                    foreach (var project in dataSet.Projects.Where(x => !x.Expired).OrderByDescending(x => x.CoolScore))
                    {
                        var contributors = AssignContributorsToProject(dataSet.Contributors, project);

                        if (contributors != null)
                        {
                            countOfEmptyCycles = 0;

                            result.Projects.Add(new ProjectsAndContributors
                            {
                                Contributors = contributors,
                                Project = project
                            });

                            Console.WriteLine($"Project {project.Name} added.");
                        }
                        else
                        {
                            countOfEmptyCycles++;
                        }
                    }

                    Console.WriteLine(dataSet.Contributors.Sum(x => x.BusyFor));
                    Console.WriteLine($"Total projects added {result.Projects.Count}/{dataSet.ProjectCount}");

                    if (dataSet.Contributors.All(x => x.BusyFor == 0) || countOfEmptyCycles >= 70000) break;

                    SimulateDay(dataSet.Contributors);

                    simulationDays++;
                }

                InputOutput.OutputData(fileName, result);
            }
        }

        private static void ScoreProjects(List<Project> dataSetProjects, int simulationDays)
        {
            foreach (var project in dataSetProjects)
            {
                var porentialScore = 0;
                
                if (simulationDays + project.CompletionDays - project.BestBefore > 0)
                {
                    porentialScore = project.Score;
                }
                else
                {
                    porentialScore = project.Score - (simulationDays + project.CompletionDays - project.BestBefore);
                }

                project.CoolScore = porentialScore / (double) project.CompletionDays;

                if (porentialScore < 1)
                {
                    project.Expired = true;
                }
                
                
                // if (simulationDays + project.CompletionDays < project.BestBefore)
                // {
                //     // project.CoolScore = (double) project.Score  / project.CompletionDays;
                // }
                // else
                // {
                //     // project.CoolScore = project.Score - simulationDays - project.CompletionDays - project.BestBefore;
                // }

                project.CoolScore = (double) project.Score / project.CompletionDays;
            }
        }


        private static void SimulateDay(List<Contributor> availableContributors)
        {
            foreach (var contributor in availableContributors)
            {
                if (contributor.BusyFor > 0)
                {
                    contributor.BusyFor--;

                    if (contributor.BusyFor == 0)
                    {
                        // Level up skill
                        var skill = contributor.Skills.FirstOrDefault(x =>
                            x.Name == contributor.RoleFilled.Name && contributor.RoleFilled.Level >= x.Level);

                        if (skill != null)
                        {
                            skill.Level++;
                        }
                    }
                }
            }
        }

        private static List<Contributor> AssignContributorsToProject(List<Contributor> availableContributors,
            Project concreteProject)
        {
            var tempContributors = new List<Contributor>();

            foreach (var role in concreteProject.Roles)
            {
                var contributor = availableContributors
                    .Where(x =>
                        x.Skills.Any(y => y.Name == role.Name && y.Level >= role.Level) && x.BusyFor == 0 &&
                        !tempContributors.Contains(x))
                    .OrderBy(x => x.Skills.Sum(y => y.Level))
                    .FirstOrDefault();

                if (contributor == null)
                    return null;

                tempContributors.Add(contributor);
            }

            for (var i = 0; i < tempContributors.Count; i++)
            {
                tempContributors[i].BusyFor = concreteProject.CompletionDays;
                tempContributors[i].RoleFilled = concreteProject.Roles[i];
            }

            concreteProject.Expired = true;

            return tempContributors;
        }
    }
}


// foreach (var fileName in DataFilesList)
//            {
//                var dataSet = InputOutput.ReadData($"data/{fileName}");
//                var resultSet = new InputOutput.ResultSet();
//
//                for (int i = 0; i < dataSet.IntCount; i++)
//                {
//                    resultSet.Intersections.Add(new InputOutput.Intersection
//                    {
//                        Id = i,
//                        GreenStreets = new Dictionary<string, InputOutput.StreetGreen>(),
//                        ScoreFinal = 0,
//                        ScoreInitial = 0
//                    });
//                }
//
//                foreach (KeyValuePair<string, Street> str in dataSet.Streets)
//                {
//                    resultSet.Intersections[str.Value.FinalIntNum].ScoreFinal++;
//                    resultSet.Intersections[str.Value.StartIntNum].ScoreInitial++;
//                }
//
//                foreach (KeyValuePair<string, Street> str in dataSet.Streets)
//                {
//                    if (str.Value.Score > 0)
//                    {
//                        Random rnd = new Random();
//                        Random rnd2 = new Random();
//                        resultSet.Intersections[str.Value.FinalIntNum].GreenStreets[str.Value.StreetName] =
//                            new InputOutput.StreetGreen
//                            {
//                                Title = str.Value.StreetName,
//                                //Sec = (int)Math.Ceiling((decimal)str.Value.Score / (decimal)(dataSet.Sec) * (decimal)rnd.Next(1000))
//                                Sec = rnd.Next(100, 500)
//                            };
//
//                        /*if(str.Value.Score > dataSet.Sec)
//                        {
//                            resultSet.Intersections[str.Value.FinalIntNum].GreenStreets[str.Value.StreetName] = new InputOutput.StreetGreen
//                            {
//                                Title = str.Value.StreetName,
//                                Sec = (int)Math.Ceiling((decimal)dataSet.Sec / (decimal)(resultSet.Intersections[str.Value.FinalIntNum].ScoreFinal))
//                            };
//                        }*/
//                    }
//                }
//
//                List<InputOutput.Intersection> intersectionsFixed = new List<InputOutput.Intersection>();
//
//                foreach (var intersection in resultSet.Intersections)
//                {
//                    if (intersection.GreenStreets.Count > 0)
//                    {
//                        foreach (var street in intersection.GreenStreets.Values)
//                        {
//                            intersection.GreenStreetsList.Add(street);
//                        }
//
//                        intersection.GreenStreetsList.Sort((x, y) => x.Sec.CompareTo(y.Sec));
//                        intersectionsFixed.Add(intersection);
//                    }
//                }
//
//                resultSet.Intersections = intersectionsFixed;
//
//                Console.WriteLine(fileName);
//
//                InputOutput.OutputData($"{fileName}-result.out", resultSet);
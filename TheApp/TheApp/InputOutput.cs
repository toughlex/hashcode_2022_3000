﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.ExceptionServices;

namespace TheApp
{
    public class InputOutput
    {
        public static DataSet ReadData(string filename)
        {
            var dataSet = new DataSet();

            using (var sr = new StreamReader(filename))
            {
                var info = sr.ReadLine().Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                dataSet.ContributorCount = int.Parse(info[0]);
                dataSet.ProjectCount = int.Parse(info[1]);

                for (var i = 0; i < dataSet.ContributorCount; i++)
                {
                    var nameAndSkillsCount = sr.ReadLine().Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                    var contributor = new Contributor
                    {
                        Name = nameAndSkillsCount[0],
                        SkillCount = int.Parse(nameAndSkillsCount[1])
                    };

                    for (var j = 0; j < contributor.SkillCount; j++)
                    {
                        var skill = sr.ReadLine().Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                        contributor.Skills.Add(new Skill
                        {
                            Name = skill[0],
                            Level = int.Parse(skill[1])
                        });
                    }

                    dataSet.Contributors.Add(contributor);
                }

                for (var i = 0; i < dataSet.ProjectCount; i++)
                {
                    var projectLine = sr.ReadLine().Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                    var project = new Project
                    {
                        Name = projectLine[0],
                        CompletionDays = int.Parse(projectLine[1]),
                        Score = int.Parse(projectLine[2]),
                        BestBefore = int.Parse(projectLine[3]),
                        RoleCount = int.Parse(projectLine[4])
                    };

                    for (var j = 0; j < project.RoleCount; j++)
                    {
                        var roleLine = sr.ReadLine().Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                        project.Roles.Add(new Skill
                        {
                            Name = roleLine[0],
                            Level = int.Parse(roleLine[1])
                        });
                    }

                    dataSet.Projects.Add(project);
                }

                return dataSet;
            }
        }

        //public class Slide
        //{
        //    public Picture Pic1;
        //    public Picture Pic2;
        //    public HashSet<string> Tags;
        //    public bool Used;

        //    public Slide(Picture pic1)
        //    {
        //        Pic1 = pic1;
        //       Tags = new HashSet<string>();
        //       Tags.UnionWith(pic1.Tags);
        //    }

        //    public override string ToString()
        //    {
        //        var result = string.Empty;
        //        if (Pic1.Orientation == "H")
        //        {

        //            return Pic1.Id.ToString();
        //        }

        //        return $"{Pic1.Id} {Pic2.Id}";
        //    }

        //    public int GetScore(Slide other)
        //    {
        //        var intersection = this.Tags.Intersect(other.Tags);
        //        var count = intersection.Count();
        //        var left = this.Tags.Count - count;
        //        var right = other.Tags.Count - count;
        //        return Math.Min(Math.Min(count, left), right);

        //    }
        //}

        public class StreetGreen
        {
            public string Title;
            public int Sec;
        }

        public class Intersection
        {
            public Intersection()
            {
                GreenStreetsList = new List<StreetGreen>();
            }

            public Dictionary<string, StreetGreen> GreenStreets;
            public List<StreetGreen> GreenStreetsList;
            public int Id;
            public int ScoreInitial;
            public int ScoreFinal;
        }

        public class ResultSet
        {
            public ResultSet()
            {
                Intersections = new List<Intersection>();
            }

            public List<Intersection> Intersections;
        }

        public static void OutputData(string fileName, Result results)
        {
            using (var outputFile = new StreamWriter($"results/{fileName}"))
            {
                outputFile.WriteLine(results.Projects.Count);

                for (var i = 0; i < results.Projects.Count; i++)
                {
                    outputFile.WriteLine($"{results.Projects[i].Project.Name}");

                    for (var j = 0; j < results.Projects[i].Contributors.Count; j++)
                    {
                        var cont = results.Projects[i].Contributors[j];

                        outputFile.Write($"{cont.Name} ");
                    }

                    outputFile.WriteLine();
                }
            }
        }
    }
}
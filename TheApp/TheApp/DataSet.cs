﻿using System.Collections.Generic;

namespace TheApp
{
    public class DataSet
    {
        public int ContributorCount { get; set; }
        public int ProjectCount { get; set; }

        public List<Project> Projects { get; set; } = new List<Project>();
        public List<Contributor> Contributors { get; set; } = new List<Contributor>();
    }

    public class Contributor
    {
        public string Name { get; set; }
        public int SkillCount { get; set; }
        public List<Skill> Skills { get; set; } = new List<Skill>();
        public int BusyFor { get; set; }
        public Skill RoleFilled { get; set; }
    }

    public class Skill
    {
        public string Name { get; set; }
        public int Level { get; set; }
    }

    public class Project
    {
        public string Name { get; set; }
        public int CompletionDays { get; set; }
        public int Score { get; set; }
        public int BestBefore { get; set; }
        public List<Skill> Roles { get; set; } = new List<Skill>();
        public int RoleCount { get; set; }
        public bool Expired { get; set; }
        public double CoolScore { get; set; }
    }

    public class Result
    {
        public List<ProjectsAndContributors> Projects { get; set; } = new List<ProjectsAndContributors>();
    }

    public class ProjectsAndContributors
    {
        public Project Project { get; set; }
        public List<Contributor> Contributors { get; set; } = new List<Contributor>();
    }
}